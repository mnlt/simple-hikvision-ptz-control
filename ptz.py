#! /usr/bin/python3

import tkinter as tk
import requests
from requests.auth import HTTPDigestAuth

HOST="host-address"
USERNAME="user"
PASSWORD="password"

def send_ptz_command(pan, tilt, zoom):
    url = f"http://{HOST}/ISAPI/PTZCtrl/channels/1/Momentary"
    payload = f'''
        <PTZData>
            <pan>{pan}</pan>
            <tilt>{tilt}</tilt>
            <zoom>{zoom}</zoom>
            <Momentary>
                <duration>500</duration>
            </Momentary>
        </PTZData>
    '''
    headers = {'Content-Type': 'application/xml'}
    auth = HTTPDigestAuth(USERNAME, PASSWORD)
    
    response = requests.put(url, data=payload, headers=headers, auth=auth, verify=False)
    
    if response.status_code == 200:
        print("PTZ command sent successfully")
    else:
        print("Failed to send PTZ command. Error code: ", response.status_code)

press_events = {
    'up': False,
    'up_left': False,
    'up_right': False,
    'down': False,
    'down_left': False,
    'down_right': False,
    'left': False,
    'right': False,
    'zoom_in': False,
    'zoom_out': False
}

def on_button_press(direction):
    speed = speed_slider.get()
    if direction == 'up':
        send_ptz_command(0, speed, 0)
    elif direction == 'up_left':
        send_ptz_command(-speed, speed, 0)
    elif direction == 'up_right':
        send_ptz_command(speed, speed, 0)
    elif direction == 'down':
        send_ptz_command(0, -speed, 0)
    elif direction == 'down_left':
        send_ptz_command(-speed, -speed, 0)
    elif direction == 'down_right':
        send_ptz_command(speed, -speed, 0)
    elif direction == 'left':
        send_ptz_command(-speed, 0, 0)
    elif direction == 'right':
        send_ptz_command(speed, 0, 0)
    elif direction == 'zoom_in':
        send_ptz_command(0, 0, 100)
    elif direction == 'zoom_out':
        send_ptz_command(0, 0, -100)
    
    press_events[direction] = True

def on_button_release(event):
    direction = event.widget.direction
    if press_events[direction]:
        press_events[direction] = False

def send_continuous_ptz_command():
    speed = speed_slider.get()
    
    if press_events['up']:
        send_ptz_command(0, speed, 0)
    elif press_events['up_left']:
        send_ptz_command(-speed, speed, 0)
    elif press_events['up_right']:
        send_ptz_command(speed, speed, 0)
    elif press_events['down']:
        send_ptz_command(0, -speed, 0)
    elif press_events['down_left']:
        send_ptz_command(-speed, -speed, 0)
    elif press_events['down_right']:
        send_ptz_command(speed, -speed, 0)
    elif press_events['left']:
        send_ptz_command(-speed, 0, 0)
    elif press_events['right']:
        send_ptz_command(speed, 0, 0)
    elif press_events['zoom_in']:
        send_ptz_command(0, 0, 100)
    elif press_events['zoom_out']:
        send_ptz_command(0, 0, -100)
    
    window.after(100, send_continuous_ptz_command)

def on_preset_button_press():
    preset_number = preset_entry.get()
    url = f"http://{HOST}/ISAPI/PTZCtrl/channels/1/presets/{preset_number}/goto"
    auth = HTTPDigestAuth(USERNAME, PASSWORD)
    response = requests.put(url, auth=auth, verify=False)
    if response.status_code == 200:
        print("PTZ preset command sent successfully")
    else:
        print("Failed to send PTZ preset command. Error code: ", response.status_code)

window = tk.Tk()
window.title("PTZ")

preset_frame = tk.Frame(window)
preset_frame.pack()

preset_label = tk.Label(preset_frame, text="Preset:")
preset_label.pack(side=tk.LEFT)

preset_entry = tk.Entry(preset_frame, width=2)
preset_entry.insert(0, '1')
preset_entry.pack(side=tk.LEFT)

preset_button = tk.Button(preset_frame, text="⚙", width=1, height=1)
preset_button.pack(side=tk.LEFT)

empty_label = tk.Label(window)
empty_label.pack()

zoom_frame = tk.Frame(window)
zoom_frame.pack()

zoom_out_button = tk.Button(zoom_frame, text="-", width=1, height=1)
zoom_out_button.direction = 'zoom_out'
zoom_out_button.pack(side=tk.LEFT)

zoom_in_button = tk.Button(zoom_frame, text="+", width=1, height=1)
zoom_in_button.direction = 'zoom_in'
zoom_in_button.pack(side=tk.LEFT)

empty_label = tk.Label(window)
empty_label.pack()

direction_frame = tk.Frame(window)
direction_frame.pack()

up_left_button = tk.Button(direction_frame, text="↖", width=3, height=2)
up_left_button.direction = 'up_left'
up_left_button.grid(row=0, column=0)

up_button = tk.Button(direction_frame, text="↑", width=3, height=2)
up_button.direction = 'up'
up_button.grid(row=0, column=1)

up_right_button = tk.Button(direction_frame, text="↗", width=3, height=2)
up_right_button.direction = 'up_right'
up_right_button.grid(row=0, column=2)

left_button = tk.Button(direction_frame, text="←", width=3, height=2)
left_button.direction = 'left'
left_button.grid(row=1, column=0)

center_button = tk.Button(direction_frame, text="", width=3, height=2, state='disabled')
center_button.grid(row=1, column=1)

right_button = tk.Button(direction_frame, text="→", width=3, height=2)
right_button.direction = 'right'
right_button.grid(row=1, column=2)

down_left_button = tk.Button(direction_frame, text="↙", width=3, height=2)
down_left_button.direction = 'down_left'
down_left_button.grid(row=2, column=0)

down_button = tk.Button(direction_frame, text="↓", width=3, height=2)
down_button.direction = 'down'
down_button.grid(row=2, column=1)

down_right_button = tk.Button(direction_frame, text="↘", width=3, height=2)
down_right_button.direction = 'down_right'
down_right_button.grid(row=2, column=2)

speed_slider = tk.Scale(window, from_=10, to=100, orient=tk.HORIZONTAL, resolution=10)
speed_slider.pack()
speed_slider.set(30)

up_button.bind("<ButtonPress-1>", lambda event: on_button_press('up'))
up_button.bind("<ButtonRelease-1>", on_button_release)

down_button.bind("<ButtonPress-1>", lambda event: on_button_press('down'))
down_button.bind("<ButtonRelease-1>", on_button_release)

left_button.bind("<ButtonPress-1>", lambda event: on_button_press('left'))
left_button.bind("<ButtonRelease-1>", on_button_release)

right_button.bind("<ButtonPress-1>", lambda event: on_button_press('right'))
right_button.bind("<ButtonRelease-1>", on_button_release)

up_left_button.bind("<ButtonPress-1>", lambda event: on_button_press('up_left'))
up_left_button.bind("<ButtonRelease-1>", on_button_release)

down_left_button.bind("<ButtonPress-1>", lambda event: on_button_press('down_left'))
down_left_button.bind("<ButtonRelease-1>", on_button_release)

up_right_button.bind("<ButtonPress-1>", lambda event: on_button_press('up_right'))
up_right_button.bind("<ButtonRelease-1>", on_button_release)

down_right_button.bind("<ButtonPress-1>", lambda event: on_button_press('down_right'))
down_right_button.bind("<ButtonRelease-1>", on_button_release)

zoom_in_button.bind("<ButtonPress-1>", lambda event: on_button_press('zoom_in'))
zoom_in_button.bind("<ButtonRelease-1>", on_button_release)

zoom_out_button.bind("<ButtonPress-1>", lambda event: on_button_press('zoom_out'))
zoom_out_button.bind("<ButtonRelease-1>", on_button_release)

preset_entry.bind("<Return>", lambda event: on_preset_button_press())
preset_button.config(command=on_preset_button_press)

window.after(100, send_continuous_ptz_command)

window.mainloop()